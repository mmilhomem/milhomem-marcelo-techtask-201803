<?php

namespace Recipe\Model;

use DateTime;
use DateTimeZone;

class IngredientModel
{
    public function getValid(DateTime $referenceDate): array
    {
        $ingredients = $this->getValidIngredients($referenceDate);

        return array_column(
            $ingredients,
            'title'
        );
    }

    private function getIngredients()
    {
        $ingredientsJson = file_get_contents(__DIR__ . '/ingredients.json');
        $ingredientsRepository = json_decode($ingredientsJson, JSON_OBJECT_AS_ARRAY);

        return $ingredientsRepository['ingredients'];
    }

    private function isWithinUseBy(array $ingredient, DateTime $referenceDate): bool
    {
        $useBy = new DateTime($ingredient['use-by'], new DateTimeZone('UTC'));

        return ($referenceDate <= $useBy);
    }

    private function getValidIngredients(DateTime $referenceDate): array
    {
        $ingredients = [];
        foreach ($this->getIngredients() as $ingredient) {
            if ($this->isWithinUseBy($ingredient, $referenceDate)) {
                $ingredients[] = $ingredient;
            }
        }

        return $ingredients;
    }

    public function getBestBeforeExpired(DateTime $referenceDate): array
    {
        $ingredients = $this->getValidIngredients($referenceDate);

        $expiredBestBeforeIngredients = array_filter($ingredients, function ($ingredient) use ($referenceDate) {
            $bestBefore = new DateTime($ingredient['best-before'], new DateTimeZone('UTC'));

            return $referenceDate >= $bestBefore;
        });

        return array_column(
            $expiredBestBeforeIngredients,
            'title'
        );
    }
}
