<?php

namespace Recipe\Model;

use DateTime;
use DateTimeZone;

class RecipeModel
{
    private $ingredientsModel;

    public function __construct(IngredientModel $ingredientsModel)
    {
        $this->ingredientsModel = $ingredientsModel;
    }

    public function lunch(DateTime $referenceDate): array
    {
        $validIngredients = $this->ingredientsModel->getValid($referenceDate);
        $validRecipes = $this->getRecipesForAvailableIngredients($validIngredients);

        $bestBeforeExpiredIngredients = $this->ingredientsModel->getBestBeforeExpired($referenceDate);

        return $this->sortByDescending($validRecipes, $bestBeforeExpiredIngredients);
    }

    private function getRecipesForAvailableIngredients(array $validIngredients)
    {
        return array_filter(
            $this->getRecipes(),
            function ($recipe) use ($validIngredients) {
                $recipeIngredients = $recipe['ingredients'];
                $missingIngredients = array_diff($recipeIngredients, $validIngredients);

                return count($missingIngredients) == 0;
            }
        );
    }

    private function getRecipes()
    {
        $recipesJson = file_get_contents(__DIR__ . '/recipes.json');
        $recipesRepository = json_decode($recipesJson, JSON_OBJECT_AS_ARRAY);

        return $recipesRepository['recipes'];
    }

    private function sortByDescending(array $recipes, array $bestBeforeExpiredIngredients)
    {
        usort($recipes, function ($leftItem, $rightItem) use ($bestBeforeExpiredIngredients) {
            $rightContainExpiredIngredients = array_intersect($rightItem['ingredients'], $bestBeforeExpiredIngredients);
            if (count($rightContainExpiredIngredients)) {
                return $keepPosition = 0;
            }

            $leftContainExpiredIngredients = array_intersect($leftItem['ingredients'], $bestBeforeExpiredIngredients);
            if (count($leftContainExpiredIngredients)) {
                return $moveItToRight = 1;
            }

            return $keepPosition = 0;
        });

        return $recipes;
    }
}
