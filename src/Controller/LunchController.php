<?php

namespace Recipe\Controller;

use DateTime;
use DateTimeZone;
use Recipe\Model\RecipeModel;
use Symfony\Component\HttpFoundation\JsonResponse;

class LunchController
{
    private $recipeModel;

    public function __construct(RecipeModel $recipeModel)
    {
        $this->recipeModel = $recipeModel;
    }

    public function indexAction()
    {
        $now = new DateTime('now', new DateTimeZone('UTC'));
        $recipes = $this->recipeModel->lunch($now);

        return new JsonResponse([
            'recipes' => $recipes
        ]);
    }
}
