<?php

namespace Recipe\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;

class IndexController
{
    public function indexAction()
    {
        return new JsonResponse(["message" => 'It works!']);
    }
}
