<?php

namespace Recipe\Test\Unit;

use DateTime;
use DateTimeZone;
use PHPUnit\Framework\TestCase;
use Recipe\Model\IngredientModel;

class IngredientModelTest extends TestCase
{
    /** @var IngredientModel */
    private $ingredientModel;

    protected function setUp()
    {
        $this->ingredientModel = new IngredientModel();
    }

    public function getValidIngredientsWithinUseByDate()
    {
        return [
            'Do not return any ingredient as will all be expired in a far future' => [
                'Years from now' => new DateTime('2080-01-01', new DateTimeZone('UTC')),
                'Expected to be' => $this->isEmpty(),
            ],
            'List some ingredients for an specific date' => [
                new DateTime('2018-03-25', new DateTimeZone('UTC')),
                $this->countOf(14),
            ],
        ];
    }

    /**
     * @dataProvider getValidIngredientsWithinUseByDate
     */
    public function testGetValidUseBy($referenceDate, $expectedConstraint)
    {
        $ingredients = $this->ingredientModel->getValid($referenceDate);

        $this->assertInternalType('array', $ingredients);
        $this->assertThat($ingredients, $expectedConstraint);
    }

    public function getValidIngredientsBestBeforeExpired()
    {
        return [
            'Get Salad at last when is past its best-before but still valid' => [
                new DateTime('2018-03-07', new DateTimeZone('UTC')),
                'Salad Dressing',
            ],
            'Get Cheese now because Salad is expired' => [
                new DateTime('2018-03-10', new DateTimeZone('UTC')),
                'Cheese',
            ],
        ];
    }

    /**
     * @dataProvider getValidIngredientsBestBeforeExpired
     */
    public function testGetValidSortedByBestBefore($referenceDate, $expectedIngredient)
    {
        $ingredients = $this->ingredientModel->getBestBeforeExpired($referenceDate);

        $this->assertInternalType('array', $ingredients);

        $lastIngredient = end($ingredients);
        $this->assertEquals($expectedIngredient, $lastIngredient);
    }
}
