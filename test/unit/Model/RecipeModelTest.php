<?php

namespace Recipe\Test\Unit;

use DateTime;
use DateTimeZone;
use PHPUnit\Framework\TestCase;
use Recipe\Model\IngredientModel;
use Recipe\Model\RecipeModel;

class RecipeModelTest extends TestCase
{
    private $ingredientModel;
    private $nowDateTime;
    /** @var RecipeModel */
    private $recipeModel;

    protected function setUp()
    {
        $this->ingredientModel = $this->createMock(IngredientModel::class);
        $this->recipeModel = new RecipeModel($this->ingredientModel);
        $this->nowDateTime = new DateTime('now', new DateTimeZone('UTC'));
    }

    public function testGetAListOfRecipesForLunch()
    {
        $recipes = $this->recipeModel->lunch($this->nowDateTime);

        $this->assertInternalType('array', $recipes);
    }

    public function testGetRecipesThatDoesNotIncludeExpiredIngredients()
    {
        $this->ingredientModel->expects($this->once())
            ->method('getValid')
            ->willReturn([
                "Ham",
                "Cheese",
                "Bread",
                "Butter",
            ]);

        $recipes = $this->recipeModel->lunch($this->nowDateTime);

        $this->assertInternalType('array', $recipes);

        $recipeTitles = array_column($recipes, 'title');
        $this->assertContains('Ham and Cheese Toastie', $recipeTitles);
        $this->assertNotContains('Fry-up', $recipeTitles);
    }

    public function getValidIngredientsWithinBestBefore()
    {
        return [
            'Get Salad at last when is past its best-before but still valid' => [
                'At this point in time' => new DateTime('2018-03-07', new DateTimeZone('UTC')),
                'This should be the last recipe' => 'Salad',
            ],
            'Get Cheese now because Salad is expired' => [
                new DateTime('2018-03-10', new DateTimeZone('UTC')),
                'Ham and Cheese Toastie',
            ],
        ];
    }

    /**
     * @dataProvider getValidIngredientsWithinBestBefore
     */
    public function testGetValidSortedByBestBefore($referenceDate, $expectedRecipeTitle)
    {
        $this->ingredientModel = new IngredientModel();
        $this->recipeModel = new RecipeModel($this->ingredientModel);

        $recipes = $this->recipeModel->lunch($referenceDate);

        $this->assertInternalType('array', $recipes);

        $lastRecipe = end($recipes);
        $this->assertEquals($expectedRecipeTitle, $lastRecipe['title']);
    }
}
