<?php

namespace Recipe\Test\Unit;

use PHPUnit\Framework\TestCase;
use Recipe\Controller\LunchController;
use Recipe\Model\RecipeModel;

class LunchControllerTest extends TestCase
{
    public function testGetLunchRecipeWillCallAModel()
    {
        $recipeModel = $this->createMock(RecipeModel::class);

        $recipeModel->expects($this->once())
            ->method('lunch');

        $lunchController = new LunchController($recipeModel);
        $lunchController->indexAction();
    }
}
