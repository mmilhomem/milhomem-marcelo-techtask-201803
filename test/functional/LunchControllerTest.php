<?php

namespace Recipe\Test\Functional;

use Recipe\Test\WebTestCase;

class LunchControllerTest extends WebTestCase
{
    public function testGetLunchRecipeListWithSuccess()
    {
        $this->client->request('GET', '/lunch');

        $this->assertTrue(
            $this->client->getResponse()->isOk(),
            sprintf('Something is wrong with the request: %s', $this->client->getResponse())
        );
    }

    public function testGetLunchRecipeRespondInJsonFormat()
    {
        $this->client->request('GET', '/lunch');

        $this->assertJson(
            $this->client->getResponse()->getContent(),
            sprintf('The response format of this endpoint must be json, got: %s', $this->client->getResponse())
        );
    }
}
