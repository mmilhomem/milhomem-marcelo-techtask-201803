<?php

namespace Recipe\Test\Functional;

use Recipe\Test\WebTestCase;

class IndexControllerTest extends WebTestCase
{
    public function testGetHomepage()
    {
        $client = $this->createClient();

        $client->request('GET', '/');

        $this->assertTrue($client->getResponse()->isOk());
    }
}
