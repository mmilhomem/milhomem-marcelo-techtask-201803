<?php

namespace Recipe\Test;

use Symfony\Component\BrowserKit\Client;

class WebTestCase extends \Silex\WebTestCase
{
    /** @var Client */
    protected $client;

    public function createApplication()
    {
        $app = require __DIR__ . '/../bootstrap/bootstrap.php';

        $this->app = $app;
        $this->client = $this->createClient();

        return $this->app;
    }
}
