#!make

build: docker-build

install:
	$(call docker_compose,up -d)

uninstall:
	$(call docker_compose,down)

.PHONY: test
test:
	$(call docker_run,php-fpm,./vendor/bin/phpunit)

help:
	@echo "available actions:"
	@echo "    build"
	@echo "        Build all container images"
	@echo "    test"
	@echo "        Test the project"
	@echo "    install"
	@echo "        Install all containers"
	@echo "    uninstall"
	@echo "        Remove all containers"

docker-build:
	$(call docker_compose,build)

DOCKER_COMPOSE_BIN = $(shell command -v docker-compose 2> /dev/null)

docker_compose = ${DOCKER_COMPOSE_BIN} -f docker/docker-compose.yaml $(1)

docker_run = $(call docker_compose,run --rm $(1) $(2))
