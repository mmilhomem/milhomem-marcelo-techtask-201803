<?php

use Recipe\Controller\IndexController;
use Recipe\Controller\LunchController;
use Recipe\Model\IngredientModel;
use Recipe\Model\RecipeModel;

$app['index.controller'] = function () {
    return new IndexController();
};

$app->get('/', 'index.controller:indexAction');

$app['ingredient.model'] = function () {
    return new IngredientModel();
};

$app['recipe.model'] = function () use ($app) {
    return new RecipeModel($app['ingredient.model']);
};

$app['lunch.controller'] = function () use ($app) {
    return new LunchController($app['recipe.model']);
};

$app->get('/lunch', 'lunch.controller:indexAction');
