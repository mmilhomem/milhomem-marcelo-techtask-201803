# Lunch recipes API

An API that will determine from a set of recipes what I can have for lunch today based on the contents of my fridge, so that I quickly decide what I’ll be having.

# Installing

This projects rely on docker and docker-compose so you need to first have this installed prior running the following commands.

To build this API you need to clone this repository and run:
```bash
make && make test
```

Then if you want to install it to see it in action run:
```bash
make install
```

# Usage

## Endpoint /lunch

Return a JSON response of the recipes that I can prepare based on the availability of ingredients in my fridge.

Any ingredient past its `best before` but still with its `use by` will be listed at the bottom of response.

Any ingredient past its `use by` will not be shown.

```http request
GET /lunch
```

```json
{
  "recipes": [
    {
      "title": "Ham and Cheese Toastie",
      "ingredients": [
        "Ham",
        "Cheese",
        "Bread",
        "Butter"
      ]
    },
    {
      "title": "Fry-up",
      "ingredients": [
        "Bacon",
        "Eggs",
        "Baked Beans",
        "Mushrooms",
        "Sausage",
        "Bread"
      ]
    }
  ]
}
```
